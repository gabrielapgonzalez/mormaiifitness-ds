import pandas as pd
import numpy as np
import logging
import sys
import json

import gspread
from oauth2client.service_account import ServiceAccountCredentials

from .output import *

logging.basicConfig(stream=sys.stdout,
                    level=logging.INFO,
                    format='%(asctime)s;%(levelname)s;%(message)s',
                    datefmt='%m/%d/%Y %I:%M:%S %p')

#VARIAVEIS---------------



#AUX FUNCS---------------
def _agg_list(_list):
    return list(_list)

def _tel_rd(tel):

    tel = str(tel)
    if tel == 'nan':
        return np.nan
    
    tel = tel.replace('(', '')\
             .replace(')', '')\
             .replace('-', '')\
             .replace(' ', '')\
             .replace('+', '')
    
    if tel == '':
        return np.nan
    
    return tel

def _agg_set(_list):
    return list(set(_list))

def _filter_adtracker(dados):
    page, form, campaign = dados
    
    if form == 'Matricula Studio Mormaii':
        return 1
    
    if campaign == 'Studios':
        return 1
        
   
    n = page.lower().find('studio')
    if n > -1:
        return 1
    else:
        return 0

def _t_unidade(x):
    
    if str(type(x)) == "<class 'float'>":
        return np.nan
    else:
        return x.lower()[:x.find('(') - 1]
    
def _t_studioid(x):
    
    if str(type(x)) == "<class 'float'>":
        return np.nan
    else:
        return x.lower()

def _cel_join(dados):
    
    tel, cel = dados

    tel = str(tel)
    cel = str(cel)
    
    if cel == 'nan':
        return tel
    else: 
        return cel

def _tel_fbleads(tel):
    
    tel = str(tel)
    
    if tel.startswith('55'):
        return tel[2:]
    else:
        return tel
        
def _try_jsonloads_first(x):
        try:
            x = json.loads(x.replace("'", '"'))
            return x[0]
        except:
            return np.nan

def _tel_pessoa(tels):
    
    try:
        out = [tel.replace('(', '').replace(')', '') for tel in tels if isinstance(tel, str)]
        return out
    except TypeError:
        return np.nan
        

def _try_list(x):
    try:
        x = json.loads(x.replace("'", '"'))
        return x
    except:
        return np.nan
    

def join_tels(x):
    x1,x2,x3 = x
    lista = [x1,x2,x3]
        
    _list = []
    for i in lista:
        if isinstance(i, list):
            _list.append(i)

    f_list = []
    for i in _list:
        f_list = f_list + i
    f_list = list(set(f_list))
    return [i for i in f_list if str(i) != 'nan']

def join_nomes(x):
    x1,x2,x3 = x
    lista = [x1,x2,x3]
        
    _list = []
    for i in lista:
        if isinstance(i, list):
            _list.append(i)

    f_list = []
    for i in _list:
        f_list = f_list + i
    f_list = list(set(f_list))
    lista_final = [i for i in f_list if str(i) != 'nan']
    
    if len(lista_final) == 0:
        return np.nan
    else:
        return lista_final[0].lower().replace('(', '').replace(')', '')

def _try_first(x):
        try:
            return x[0]
        except:
            return np.nan

#MAIN FUNCS---------------

def process_base_pacto(BASE_PACTO_NPY_PATH):
    
    dict_pacto = np.load(BASE_PACTO_NPY_PATH, allow_pickle = True).tolist()
    
    #ATRIBUIR DFS
    df_cliente = dict_pacto['cliente_0']
    df_pessoa = dict_pacto['pessoa_0']
    df_contrato= dict_pacto['contrato_0']
    df_cmodalidade = dict_pacto['contratomodalidade_0']
    df_endereco = dict_pacto['endereco_0']
    df_cidade = dict_pacto['cidade_0']
    df_telefone = dict_pacto['telefone_0']
    df_email = dict_pacto['email_0']
    df_colaborador = dict_pacto['colaborador_0']
    df_aula = dict_pacto['alunohorarioturma_0']
    df_cmodalidadeturma = dict_pacto['contratomodalidadeturma_0']
    df_turma = dict_pacto['turma_0']
    df_operacao = dict_pacto['contratooperacao_0']
    
    #GENERATE PESSOAS
    df_cliente_t = (df_cliente[["codigo", "pessoa", "situacao", "empresa", "matricula"]]
                  .rename({'codigo': 'id_cliente',
                          'pessoa': 'id_pessoa',
                          'empresa': 'id_unidade'}, axis = 1)
                   .groupby('id_pessoa').first())
    
    df_endereco_t = (df_endereco[['bairro', 'cep', 'complemento', 'endereco', 'ltdlng', 'numero', 'pessoa']]
                .rename({'pessoa': 'id_pessoa'}, axis = 1)
                .groupby('id_pessoa').first())
    
    df_telefone_t = (df_telefone[['numero', 'pessoa']]
                 .rename({'numero': 'telefone', 'pessoa': 'id_pessoa'}, axis = 1)
                 .groupby('id_pessoa').agg(_agg_list))
    
    df_email_t = (df_email[['email', 'pessoa']]
                 .rename({'pessoa': 'id_pessoa'}, axis = 1)
                 .groupby('id_pessoa').agg(_agg_list))
    
    df_cidade_t = (df_cidade[['codigo','nomesemacento']]
               .rename({'codigo': 'id_cidade',
                       'nomesemacento': 'cidade'}, axis =1)
               .groupby('id_cidade').first())
    
    df_pessoa_f = (df_pessoa[["codigo", "nome", "cfp", "datanasc", "sexo", "datacadastro", "cidade"]]
                  .rename({'codigo': 'id_pessoa', 'cfp': 'cpf', 'cidade': 'id_cidade'}, axis = 1)
                  .set_index('id_pessoa')
                  .join(df_cliente_t, how = 'left')
                  .join(df_endereco_t, how = 'left')
                  .join(df_telefone_t, how = 'left')
                  .join(df_email_t, how = 'left')
                  .reset_index()
                  .set_index('id_cidade')
                  .join(df_cidade_t, how = 'left')
                  .set_index('id_pessoa'))
    
    #GENERATE CONTRATOS
    
    df_cmodalidade_t = (df_cmodalidade[['contrato', 'vezessemana']]
                    .rename({'contrato': 'id_contrato'}, axis = 1)
                    .groupby('id_contrato').agg(_agg_list))
    
    df_contrato_f = (df_contrato[["codigo", "datalancamento", "datamatricula", "vigenciade", 
                         "vigenciaate", "valorfinal", "situacao", "pessoa", 
                         "vigenciaateajustada", "consultor", "empresa"]]
                   .rename({'codigo': 'id_contrato', 'pessoa': 'id_pessoa', 'consultor': 'id_colaborador_consultor', 'empresa': 'id_unidade'}, axis =1)
                   .set_index('id_contrato')
                   .join(df_cmodalidade_t, how = 'left'))
    
    #GENERATE FUNCIONARIOS
    df_pessoa_t = (df_pessoa[['codigo', 'nome']]
              .rename({'codigo': 'id_pessoa'}, axis =1)
              .set_index('id_pessoa'))
    
    df_colaborador_f = (df_colaborador[['codigo', 'pessoa', 'empresa']]
                    .rename({'codigo': 'id_colaborador',
                            'pessoa': 'id_pessoa',
                            'empresa': 'id_unidade'}, axis =1)
                    .set_index('id_pessoa')
                    .join(df_pessoa_t, how = 'left')
                    .reset_index()
                    .groupby('id_pessoa').agg({'id_colaborador': _agg_list, 'id_unidade': _agg_list, 'nome': 'last'}))

    #GENERATE AULAS EXPERIMENTAIS
    df_aula_f = (df_aula[['aulaexperimental', 'cliente', 'dia']]
             .rename({'cliente': 'id_cliente',
                     'dia': 'data'}, axis = 1)
             .set_index('id_cliente')
             .join(df_cliente.set_index('codigo')['pessoa'], how='left')
             .rename({'pessoa': 'id_pessoa'}, axis =1))
    
    #FILTER COLABS DE PESSOAS
    _set_colab = set(df_colaborador.pessoa.values)
    df_pessoa_f = df_pessoa_f.reset_index()
    df_pessoa_f['colaborador'] = df_pessoa_f['id_pessoa'].apply(lambda x: 1 if x in _set_colab else 0)
    df_pessoa_f = (df_pessoa_f[df_pessoa_f.colaborador == 0]
                   .set_index('id_pessoa')
                   .drop('colaborador', axis = 1))
    
    #MOVIMENTAÇÃO CONTRATOS
    _ops = [
        'TR',
        'CA',
        'AT'
    ]
    
    df_operacao['filter_tipo'] = df_operacao['tipooperacao'].apply(lambda x: 1 if x in _ops else 0)
    
    df_operacao_f = (df_operacao[df_operacao.filter_tipo == 1]
                 [['contrato', 'tipooperacao','datafimefetivacaooperacao','datainicioefetivacaooperacao']]
                 .rename({'contrato': 'id_contrato', 'datafimefetivacaooperacao': 'data_fim', 
                          'datainicioefetivacaooperacao': 'data_inicio'}, axis = 1))
        
    
    return {'pessoa': df_pessoa_f, 
            'contrato': df_contrato_f, 
            'colaborador': df_colaborador_f, 
            'aula_exp': df_aula_f,
            'mov_contrato': df_operacao_f}
    
def process_base_pacto_bv(BASE_PACTO_BV_NPY_PATH):
    
    dict_pacto_bv = np.load(BASE_PACTO_BV_NPY_PATH, allow_pickle = True).tolist()
    
    df_perguntacliente = dict_pacto_bv["perguntacliente_0"] 
    df_questionariocliente = dict_pacto_bv['questionariocliente_0']
    df_questionarioperguntacliente = dict_pacto_bv["questionarioperguntacliente_0"]
    df_respostapergcliente = dict_pacto_bv["respostapergcliente_0"]
    
    df_questionariocliente_t = (df_questionariocliente[['cliente', 'codigo', 'consultor', 'data', 'tipobv', 'questionario']]
                            .rename({'cliente': 'id_cliente', 'codigo': 'id_questionariocliente',
                                    'consultor': 'id_consultor', 'tipobv': 'id_tipobv', 
                                    'questionario': 'id_questionario'}, axis = 1)
                            .set_index('id_questionariocliente'))
    
    df_questionarioperguntacliente_t = (df_questionarioperguntacliente[['perguntacliente', 'questionariocliente']]
                                    .rename({'perguntacliente': 'id_perguntacliente', 'questionariocliente': 'id_questionariocliente'}, axis =1)
                                   .set_index('id_questionariocliente'))
    
    df_questionariocliente_t2 = df_questionarioperguntacliente_t.join(df_questionariocliente_t, how = 'left')
    
    df_perguntacliente_t = (df_perguntacliente.rename({'codigo': 'id_perguntacliente', 'descricao': 'descricaopergunta'}, axis = 1)
                        .set_index('id_perguntacliente'))
    
    df_questionariocliente_t2 = (df_questionariocliente_t2.reset_index()
                             .set_index('id_perguntacliente'))
    
    df_perguntacliente_t2 = df_perguntacliente_t.join(df_questionariocliente_t2, how ='left')
    
    df_respostapergcliente_t = (df_respostapergcliente
                            [['descricaorespota', 'perguntacliente', 'respostaopcao', 'respostatextual']]
                            [df_respostapergcliente['respostaopcao']]
                            .drop('respostaopcao', axis =1)
                            .rename({'perguntacliente': 'id_perguntacliente'}, axis =1)
                            .groupby('id_perguntacliente').agg({'descricaorespota': _agg_list, 'respostatextual': 'first'}))
    
    df_bv_f = (df_perguntacliente_t2.join(df_respostapergcliente_t, how= 'left')
               .dropna(subset =['descricaorespota'])
               .drop('respostatextual', axis = 1))
               
    df_multipla = (df_bv_f[df_bv_f.multipla].descricaorespota
                   .apply(lambda x: {i: 1 for i in x})
                   .apply(pd.Series))
    
    return {'bv': df_bv_f, 
            'bv multi': df_multipla}

def process_leads(BASE_FBLEADS_NPY_PATH, BASE_ADTRACKER_NPY_PATH, BASE_RD_NPY_PATH):
    
    #LEITURA DE DADOS
    df_fbleads = np.load(BASE_FBLEADS_NPY_PATH, allow_pickle = True).tolist()['base_fbleads']
    df_adtracker = np.load(BASE_ADTRACKER_NPY_PATH, allow_pickle = True).tolist()['adtracker']
    df_rd = np.load(BASE_RD_NPY_PATH, allow_pickle = True).tolist()['rd']
    
    #PRE PROCESSAMENTO
    df_fbleads_f = _ppl_fbleads(df_fbleads).groupby('email_fbleads').agg(_agg_set)
    df_adtracker_f = _ppl_adtracker(df_adtracker).groupby('email_adtracker').agg(_agg_set)
    df_rd_f = _ppl_rd(df_rd).groupby('email_rd').agg(_agg_set)
    
    #JOIN
    df_fbleads_f['fbleads'] = [1 for i in range(df_fbleads_f.shape[0])]
    df_adtracker_f['adtracker'] = [1 for i in range(df_adtracker_f.shape[0])]
    df_rd_f['rd'] = [1 for i in range(df_rd_f.shape[0])] 
    
    df_leads_f = df_fbleads_f.join(df_adtracker_f, how = 'outer')
    df_leads_f = df_leads_f.join(df_rd_f, how = 'outer')
    
    df_leads_f['fbleads'] = df_leads_f['fbleads'].fillna(0)
    df_leads_f['adtracker'] = df_leads_f['adtracker'].fillna(0)
    df_leads_f['rd'] = df_leads_f['rd'].fillna(0)
    
    df_leads_f = df_leads_f.reset_index().rename({'index': 'email'}, axis =1)
    
    return {'leads': df_leads_f}
    
def _ppl_rd(df_rd):
    
    df_rd_f = (df_rd[['Data da Conversão', 'Email', 'Identificador', 'Nome', 'Telefone' , 'Celular']]
        .rename({'Data da Conversão': 'data'}, axis=1))
    
    df_rd_f.columns = [x.lower()+'_rd' for x in list(df_rd_f.columns)]
    
    df_rd_f.email_rd = df_rd_f.email_rd.apply(lambda x: x.lower())
    
    df_rd_f.telefone_rd = df_rd_f[['telefone_rd', 'celular_rd']].apply(_cel_join, axis = 1)
    df_rd_f.telefone_rd = df_rd_f.telefone_rd.apply(_tel_rd).apply(_tel_fbleads)
    df_rd_f = df_rd_f.drop('celular_rd', axis = 1)
    
    return df_rd_f

def _ppl_fbleads(df_fbleads):
    df_fbleads_f = df_fbleads.copy()
    df_fbleads_f.columns = [x.lower()+'_fbleads' for x in list(df_fbleads_f.columns)]
    df_fbleads_f.telefone_fbleads = df_fbleads_f.telefone_fbleads.apply(_tel_fbleads)
    df_fbleads_f.email_fbleads = df_fbleads_f.email_fbleads.apply(lambda x: x.lower())
    
    return df_fbleads_f


def _ppl_adtracker(df_adtracker):
    
    df_adtracker_f = df_adtracker.copy()
    
    df_adtracker_f['form_data'] = df_adtracker_f['form_data'].apply(lambda x: {_dict['name']: _dict['value'] 
                                                                    for _dict in json.loads(x)})
    
    df_adtracker_f['filter'] = df_adtracker_f[['page', 'form', 'campaign']].apply(_filter_adtracker, axis = 1)
    df_adtracker_f = df_adtracker_f[df_adtracker_f['filter'] == 1]
    
    phone_cols = ['personal_phone', 'franquia_telefone', 'get_franquia_telefone', 'phone', 'telefone', 'franquia_telefone_2']
    studio_cols = ['studio_id', 'sutdio', 'unidade']
    
    df_adtracker_f = df_adtracker_f.join(df_adtracker_f.form_data.apply(pd.Series)[phone_cols+studio_cols])
    
    #TRTAMENTO DADOS FORM DATA
    df_adtracker_f.personal_phone = df_adtracker_f.personal_phone.apply(_tel_rd)
    df_adtracker_f.franquia_telefone = df_adtracker_f.franquia_telefone.apply(_tel_rd)
    df_adtracker_f.get_franquia_telefone = df_adtracker_f.get_franquia_telefone.apply(_tel_rd)
    df_adtracker_f.phone = df_adtracker_f.phone.apply(_tel_rd)
    df_adtracker_f.telefone = df_adtracker_f.telefone.apply(_tel_rd)
    df_adtracker_f.franquia_telefone_2 = df_adtracker_f.franquia_telefone_2.apply(_tel_rd)
    
    df_adtracker_f['studio_id'] = df_adtracker_f['studio_id'].apply(_t_studioid)
    df_adtracker_f['sutdio'] = df_adtracker_f['sutdio'].apply(_t_studioid)
    df_adtracker_f['unidade'] = df_adtracker_f['unidade'].apply(_t_unidade)
    
    df_adtracker_f['telefone'] = (df_adtracker_f[phone_cols].apply(lambda x: list(set(x)), axis =1)
                                .apply(lambda x: [i for i in x if str(i) != 'nan'])
                               .apply(lambda x: x[0] if len(x) > 0 else np.nan))
    
    df_adtracker_f['studio'] = (df_adtracker_f[studio_cols].apply(lambda x: list(set(x)), axis =1)
                                .apply(lambda x: [i for i in x if str(i) != 'nan'])
                               .apply(lambda x: x[0] if len(x) > 0 else np.nan))
    
    df_adtracker_f['person_email'] = df_adtracker_f['person_email'].apply(lambda x: x.lower())
    
    col_adtracker = ['created_at','person_name', 'person_email', 'form', 'media', 'action', 'campaign','telefone', 'studio']
    df_adtracker_f = df_adtracker_f[col_adtracker]
    
    df_adtracker_f.columns = ['data', 'nome', 'email', 'formulario', 'canal', 'peca', 'campanha', 'telefone', 'studio']
    df_adtracker_f.columns = [x+'_adtracker' for x in list(df_adtracker_f.columns)]
    
    return df_adtracker_f
    

def get_pessoa_leads(df_leads, df_pessoa):
    
    #TRATAMENTO DF PESSOA----------------------------------------------------
    
    df_pessoa['pacto'] = [1 for i in range(df_pessoa.shape[0])]

    #GET FIRST EMAIL
    df_pessoa.email = df_pessoa.email.apply(_try_first)
    
    #LOWER NOME
    df_pessoa.nome = df_pessoa.nome.apply(lambda x: x.lower())
    
    #TRATAR TELEFONES PACTO
    #df_pessoa.telefone = df_pessoa.telefone.apply(_try_list)
    df_pessoa.telefone = df_pessoa.telefone.apply(_tel_pessoa)
    df_pessoa.nome = df_pessoa.nome.apply(lambda x: x.lower())
    df_pessoa_pdseries = df_pessoa.telefone.apply(pd.Series)
    
    #TRATAMENTO LEADS----------------------------------------------------------
    df_leads['telefones'] = df_leads[['telefone_fbleads','telefone_rd','telefone_adtracker']].apply(join_tels, axis =1)
    df_leads['nome'] = df_leads[['nome_rd','nome_adtracker','nome_fbleads']].apply(join_nomes, axis =1)
    
    
    def find_pessoa_email(email):
        
        df_pessoa_f = df_pessoa[df_pessoa.email == email].copy()
        if df_pessoa_f.shape[0] == 0:
            return np.nan
        else:
            return df_pessoa_f.id_pessoa.iloc[0]
        
        return np.nan
    
    def find_pessoa_telefone(telefones):
        
        if isinstance(telefones, float):
            return np.nan
        
        for telefone in telefones:
            for col in df_pessoa_pdseries.columns:
                df_pessoa_filtered = df_pessoa_pdseries[df_pessoa_pdseries[col] == telefone]
                
                if df_pessoa_filtered.shape[0] > 0:
                    _index = df_pessoa_filtered.index[0]
                    return df_pessoa.id_pessoa.loc[_index]

        return np.nan
        
    def find_pessoa_nome(dados):
         
        nome, telefones = dados
        
        if isinstance(telefones, float):
            return np.nan
                
        nome_list = nome.split(' ')
        if len(nome_list) < 2:
            return np.nan
        else:
            nome1 = nome_list[0]
            nome2 = nome_list[-1]
            
            filter_df_pessoa = df_pessoa[(df_pessoa.nome.str.contains(nome1)) & (df_pessoa.nome.str.contains(nome2))]
            
            if filter_df_pessoa.shape[0] == 1:
                if isinstance(filter_df_pessoa.telefone.iloc[0], float):
                    return np.nan
                
                if str(filter_df_pessoa.telefone.iloc[0][0])[:2] == str(telefones[0])[:2]:
                    return filter_df_pessoa.id_pessoa.iloc[0]
            else:
                return np.nan
                
        return np.nan
        
    
    def find_pessoa_final(dados):
        email, tel = dados
        
        if email > 0:
            return email
            
        if tel > 0:
            return tel

        return np.nan
        
    #APPLY FUNCTION DE LINK COM A PACTO------------------------------------------
    logging.info('start find email')
    df_leads['find_email'] = df_leads['email'].apply(find_pessoa_email)
    logging.info('start find telefone')
    df_leads['find_telefone'] = df_leads['telefones'].apply(find_pessoa_telefone)
    
    df_leads['find_pessoa'] = df_leads[['find_email', 'find_telefone']].apply(find_pessoa_final, axis =1)
    
    return {'leads': df_leads}

def run_preprocess(find_pessoa = False):

    logging.info('START BASE PACTO')
    dict_pacto = process_base_pacto('tmp-data/base_pacto.npy')
    logging.info('START BASE BV')
    dict_bv = process_base_pacto_bv('tmp-data/base_pacto_bv.npy')
    logging.info('START BASE LEADS')
    dict_lead = process_leads(BASE_FBLEADS_NPY_PATH = 'tmp-data/base_fbleads.npy',
                                BASE_ADTRACKER_NPY_PATH = 'tmp-data/base_adtracker.npy', 
                                BASE_RD_NPY_PATH = 'tmp-data/base_rd.npy')
    if find_pessoa:
        logging.info('START GET PESSOA')
        dict_lead = get_pessoa_leads(dict_lead['leads'].copy(), 
                                    dict_pacto['pessoa'].copy().reset_index())

    output2excel([dict_pacto, dict_bv, dict_lead], 'tmp-data/final_output.xlsx')