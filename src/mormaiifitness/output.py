import pandas as pd
import numpy as np
import logging
import sys

import gspread
from oauth2client.service_account import ServiceAccountCredentials

logging.basicConfig(stream=sys.stdout,
                    level=logging.INFO,
                    format='%(asctime)s;%(levelname)s;%(message)s',
                    datefmt='%m/%d/%Y %I:%M:%S %p')


#VARIAVEIS----------

_SCOPE = ['https://spreadsheets.google.com/feeds',
         'https://www.googleapis.com/auth/drive']
CREDENTIALS_KEY = '/home/gabriela/Documents/repositorios/mormaiifitness-ds/data/mormaiifitness-8a226de8a311.json'

#FUNCOES------------

def output2drive(SHEET_NAME, DF_SHEET_TUPLES):
    
    #CONFIGURANDO CREDENCIAIS
    credentials = ServiceAccountCredentials.from_json_keyfile_name(CREDENTIALS_KEY, _SCOPE)
    gc = gspread.authorize(credentials)
    
    sht = gc.open(SHEET_NAME)
    
    for i in range(len(DF_SHEET_TUPLES)):
        df, worksheet_name = DF_SHEET_TUPLES[i]
        
        #try:
        worksheet = sht.worksheet(worksheet_name)
        sht.del_worksheet(worksheet)
        #except:
        #    pass
        #try:
        worksheet = sht.add_worksheet(title=worksheet_name, rows=df.shape[0]+10, cols=df.shape[1] + 10)
        #except:
        #    print('Erro ao criar planilha')
        #    continue
        
        for i in range(df.shape[1]):
            worksheet.update_cell(1, i+1, str(df.columns[i]))
            for j in range(df.shape[0]):
                worksheet.update_cell(j+2, i+1, str(df.iloc[j, i]))

def output2excel(DICT_LIST,output_path):
    
    f_dict = {}
    for _dict in DICT_LIST:
        f_dict.update(_dict)
        
    # Create a Pandas Excel writer using XlsxWriter as the engine.
    writer = pd.ExcelWriter(output_path, engine='xlsxwriter')

    # Write each dataframe to a different worksheet.
    for dfname in f_dict.keys():
        print(f'Writing {dfname}')
        f_dict[dfname].to_excel(writer, sheet_name=dfname)

    # Close the Pandas Excel writer and output the Excel file.
    writer.save()


def read_output2excel(FILE_NAME):
    
    xl = pd.ExcelFile(FILE_NAME)
    dict_dfs = {i: xl.parse(i)
            for i in xl.sheet_names}
    
    return dict_dfs
    