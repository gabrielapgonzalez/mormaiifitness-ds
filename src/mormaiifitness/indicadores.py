import pandas as pd
import numpy as np
import logging
import sys
from datetime import datetime

from .output import *

import warnings
warnings.filterwarnings("ignore")

logging.basicConfig(stream=sys.stdout,
                    level=logging.INFO,
                    format='%(asctime)s;%(levelname)s;%(message)s',
                    datefmt='%m/%d/%Y %I:%M:%S %p')

def output2excel(DICT_LIST,output_path):
    
    f_dict = {}
    for _dict in DICT_LIST:
        f_dict.update(_dict)
        
    # Create a Pandas Excel writer using XlsxWriter as the engine.
    writer = pd.ExcelWriter(output_path, engine='xlsxwriter')

    # Write each dataframe to a different worksheet.
    for dfname in f_dict.keys():
        print(f'Writing {dfname}')
        f_dict[dfname].to_excel(writer, sheet_name=dfname)

    # Close the Pandas Excel writer and output the Excel file.
    writer.save()

def preprocess_indicador(dict_dfs, unidade):
    
    #CONTRATO
    df_contrato = dict_dfs['contrato']
    df_contrato_f= df_contrato[df_contrato['id_unidade'] == unidade].copy()
    df_contrato_f['vigenciade_month'] = df_contrato_f['vigenciade'].apply(lambda x: x.month)
    df_contrato_f['vigenciade_year'] = df_contrato_f['vigenciade'].apply(lambda x: x.year)
    df_contrato_f['vigenciaateajustada_month'] = df_contrato_f['vigenciaateajustada'].apply(lambda x: x.month)
    df_contrato_f['vigenciaateajustada_year'] = df_contrato_f['vigenciaateajustada'].apply(lambda x: x.year)
    
    def _contratosanteriores_indicadores(dados):
        id_pessoa, vigenciade = dados

        values = (df_contrato_f[(df_contrato_f.id_pessoa == id_pessoa)&
                                       (df_contrato_f.vigenciade < vigenciade)]
                  .sort_values('vigenciade', ascending = False)
                  ['id_contrato']
                  .values)
        return list(values)

    def _contratosposteriores_indicadores(dados):
        id_pessoa, vigenciade = dados
        
        values = (df_contrato_f[(df_contrato_f.id_pessoa == id_pessoa)&
                                       (df_contrato_f.vigenciade > vigenciade)]
                  .sort_values('vigenciade')
                  ['id_contrato']
                  .values)
        return list(values)
    
    df_contrato_f['contratos_anteriores'] = df_contrato[['id_pessoa', 'vigenciade']].apply(_contratosanteriores_indicadores, axis =1)
    df_contrato_f['contratos_posteriores'] = df_contrato[['id_pessoa', 'vigenciade']].apply(_contratosposteriores_indicadores, axis =1)
    
    #PESSOA
    df_pessoa = dict_dfs['pessoa']
    df_pessoa_f= df_pessoa[df_pessoa['id_unidade'] == unidade].copy()
    df_pessoa_f['datacadastro_month'] = df_pessoa_f.datacadastro.apply(lambda x: x.month)
    df_pessoa_f['datacadastro_year'] = df_pessoa_f.datacadastro.apply(lambda x: x.year)
    
    
    #MOV
    df_mov = dict_dfs['mov_contrato']
    serie_idunidade = df_contrato.set_index('id_contrato')['id_unidade']
    df_mov = df_mov.set_index('id_contrato').join(serie_idunidade)
    df_mov = df_mov.reset_index()
    df_mov_f = df_mov[df_mov.id_unidade == unidade]
    df_mov_f['datainicio_month'] = df_mov_f['data_inicio'].apply(lambda x: x.month)
    df_mov_f['datainicio_year'] = df_mov_f['data_inicio'].apply(lambda x: x.year)
    
    return {'contrato': df_contrato_f,
            'pessoa': df_pessoa_f,
            'mov_contrato': df_mov_f}

def indicador_alunos_ativos(df_contrato, df_pessoa, mes, ano, unidade):
    
    contratos_ativos = (df_contrato[(df_contrato['vigenciaateajustada'] > datetime(ano, mes+1,1)) &
                          (df_contrato['vigenciade'] < datetime(ano, mes+1,1))]
                          .groupby('id_pessoa')
                          .last())
    
    alunos_ativos = (df_pessoa
                     .set_index('id_pessoa')
                     .join(contratos_ativos, how = 'right', rsuffix = '_contrato')
                     .reset_index())
    
    return {'dataframe_alunos_ativos': alunos_ativos,
            'indicador_alunos_ativos': contratos_ativos.shape[0]}


def indicador_cancelado_ajuste(df_pessoa, df_mov, df_contrato, mes, ano, unidade):
    
    cancelados = list(df_mov[(df_mov.id_unidade == unidade) &
                       (df_mov.datainicio_month == mes) &
                       (df_mov.datainicio_year == ano) &
                       (df_mov.tipooperacao == 'CA')].id_contrato.values)
    
    df_cancelados = df_contrato.set_index('id_contrato').loc[cancelados]
    
    if df_cancelados.shape[0] == 0:
        df_cancelados['ajuste'] = []
        return {'dataframe_cancelados': df_cancelados,
            'indicador_cancelados': df_cancelados.shape[0],
            'dataframe_ajustes': df_cancelados,
            'indicador_ajustes': df_cancelados.shape[0]}
    
    def _ajuste(dados):
        
        contratos_posteriores, vigenciaateajustada = dados

        if len(contratos_posteriores) == 0:
            return 0

        _id_contrato = contratos_posteriores[0]

        vigenciade_contratopost = df_contrato.set_index('id_contrato').loc[_id_contrato]['vigenciade']

        delta = (vigenciade_contratopost - vigenciaateajustada).days

        if delta < 29:
            return 1
        else:
            return 0
    
    df_cancelados['ajuste'] = df_cancelados[['contratos_posteriores', 'vigenciaateajustada']].apply(_ajuste, axis =1)
    
    df_cancelados = df_cancelados.reset_index().set_index('id_pessoa')
    
    df_final = (df_pessoa
                .set_index('id_pessoa')
                .join(df_cancelados, how = 'right', rsuffix = '_contrato')
                .reset_index())
    
    return {'dataframe_cancelados': df_final[df_final.ajuste == 0],
            'indicador_cancelados': df_final[(df_final.ajuste == 0) & (df_final.valorfinal > 0)].shape[0],
            'dataframe_ajustes': df_final[df_final.ajuste == 1],
            'indicador_ajustes': df_final[df_final.ajuste == 1].shape[0]}

def indcador_nova_matriculas(df_pessoa, df_contrato, mes, ano, unidade):
    
    df_contrato = df_contrato[(df_contrato.vigenciade_month == mes) &
                             (df_contrato.vigenciade_year == ano)]
    df_contrato['nova_matricula'] = df_contrato['contratos_anteriores'].apply(lambda x: 1 if len(x) == 0 else 0)
    
    df_final = (df_pessoa
                .set_index('id_pessoa')
                .join(df_contrato[df_contrato['nova_matricula'] == 1].set_index('id_pessoa'), how = 'right', rsuffix = '_contrato')
               .reset_index())
    
    return {'dataframe_nova_matricula': df_final,
           'indicador_nova_matricula': df_final[df_final.valorfinal > 0].shape[0]}
           

def indicador_rematricula_renovacao(df_pessoa, df_contrato, df_mov, mes, ano, unidade):

    df_contrato_vigenciaate = df_contrato[(df_contrato.vigenciaateajustada_month == mes) &
                                          (df_contrato.vigenciaateajustada_year == ano)]
    
    df_contrato_vigenciade = df_contrato[(df_contrato.vigenciade_month == mes) &
                                          (df_contrato.vigenciade_year == ano)]
    
    cancelados = list(df_mov[(df_mov.id_unidade == unidade) &
                       (df_mov.datainicio_month == mes) &
                       (df_mov.datainicio_year == ano) &
                       (df_mov.tipooperacao == 'CA')].id_contrato.values)

    naocancelados = [contrato for contrato in df_contrato_vigenciaate.id_contrato.values 
                    if contrato not in cancelados]
                    
    df_contrato_vigenciaate_naocancelado = df_contrato_vigenciaate.set_index('id_contrato').loc[naocancelados]
    
    def _renovado(dados):
        
        contratos_posteriores, vigenciaateajustada = dados
        
        if len(contratos_posteriores) == 0:
            return 0
        
        _id_contrato = contratos_posteriores[0]
        
        vigenciade_contratopost = df_contrato.set_index('id_contrato').loc[_id_contrato]['vigenciade']
        
        delta = (vigenciade_contratopost - vigenciaateajustada).days
        
        if delta < 29:
            return 1
        else:
            return 0
    
    df_contrato_vigenciaate_naocancelado['renovado'] = df_contrato_vigenciaate_naocancelado[['contratos_posteriores',  'vigenciaateajustada']].apply(_renovado, axis =1)
    
    def _rematricula(dados):
        
        contratos_anteriores, vigenciade = dados
        
        if len(contratos_anteriores) == 0:
            return 0
        
        _id_contrato = contratos_anteriores[0]
        
        vigenciaate_anterior = df_contrato.set_index('id_contrato').loc[_id_contrato]['vigenciaateajustada']
        
        delta = (vigenciade - vigenciaate_anterior).days
        
        if delta > 28:
            return 1
        else:
            return 0
            
    df_contrato_vigenciade['rematricula'] = df_contrato_vigenciade[['contratos_anteriores', 'vigenciade']].apply(_rematricula, axis =1)
    
    def _reajuste(dados):
        
        contratos_anteriores, vigenciade = dados
        
        if len(contratos_anteriores) == 0:
            return 0
        
        _id_contrato = contratos_anteriores[0]
        
        if _id_contrato not in df_mov[df_mov.tipooperacao == 'CA'].id_contrato.values:
            return 0
        
        vigenciaate_anterior = df_contrato.set_index('id_contrato').loc[_id_contrato]['vigenciaateajustada']
        
        delta = (vigenciade - vigenciaate_anterior).days
        
        if delta < 29:
            return 1
        else:
            return 0
    
    df_contrato_vigenciade['reajuste'] = df_contrato_vigenciade[['contratos_anteriores', 'vigenciade']].apply(_reajuste, axis = 1)
    
    df_contrato_vigenciade = (df_pessoa
                    .set_index('id_pessoa')
                    .join(df_contrato_vigenciade.set_index('id_pessoa'), how = 'right', rsuffix = '_contrato')
                    .reset_index())
    
    df_contrato_vigenciaate_naocancelado = (df_pessoa
                    .set_index('id_pessoa')
                    .join(df_contrato_vigenciaate_naocancelado.reset_index().set_index('id_pessoa'), how = 'right', rsuffix = '_contrato')
                    .reset_index())
    
    return {
            'indicador_renovados': df_contrato_vigenciaate_naocancelado[(df_contrato_vigenciaate_naocancelado.renovado == 1) & (df_contrato_vigenciaate_naocancelado.valorfinal > 0)].shape[0],
            'indicador_nrenovados': df_contrato_vigenciaate_naocancelado[(df_contrato_vigenciaate_naocancelado.renovado == 0) & (df_contrato_vigenciaate_naocancelado.valorfinal > 0)].shape[0],
            'dataframe_rematricula': df_contrato_vigenciade[df_contrato_vigenciade.rematricula == 1],
            'indicador_rematricula': df_contrato_vigenciade[(df_contrato_vigenciade.rematricula == 1) & (df_contrato_vigenciade.valorfinal > 0)].shape[0],
            'dataframe_reajuste': df_contrato_vigenciade[df_contrato_vigenciade.reajuste == 1],
            'indicador_reajuste': df_contrato_vigenciade[(df_contrato_vigenciade.reajuste == 1) & (df_contrato_vigenciade.valorfinal > 0)].shape[0],
            'dataframe_possiveisrenovacoes':  df_contrato_vigenciaate_naocancelado,
            'indicador_possiveisrenovacoes': df_contrato_vigenciaate_naocancelado[df_contrato_vigenciaate_naocancelado.valorfinal >0].shape[0]
            }

def indicador_visitantes(df_pessoa, mes, ano, unidade):
    
    df_visitantes = df_pessoa[(df_pessoa['datacadastro_month'] == mes) & 
                              (df_pessoa['datacadastro_year'] == ano) &
                              (df_pessoa['id_unidade'] == unidade)]
    
    return {'dataframe_visitantes': df_visitantes,
            'indicador_visitantes': df_visitantes.shape[0]}
            

def posprocess(dict_list, mes, ano):
    
    f_dict = {}
    for _dict in dict_list:
        f_dict.update(_dict)
        
    f_dict_dataframe = {key: f_dict[key] for key in f_dict if key.startswith('dataframe')}

    df_ajuste_final = (pd.concat([f_dict['dataframe_ajustes'], 
                             f_dict['dataframe_reajuste']], sort=False)
                   .sort_values('id_pessoa')
                   .drop(['rematricula', 'reajuste','ajuste'], axis = 1))
    
    f_dict_dataframe.pop('dataframe_ajustes')
    f_dict_dataframe.pop('dataframe_reajuste')
    
    f_dict_dataframe['dataframe_ajustes'] = df_ajuste_final
    
    for key in f_dict_dataframe.keys():
        df = f_dict_dataframe[key].assign(mes=[mes]*f_dict_dataframe[key].shape[0],
                                         ano= [ano]*f_dict_dataframe[key].shape[0])
        
        l = list(df.columns)
        cols = l[len(l)-2:len(l)+1]+l[0: -2]
        df = df[cols]
        f_dict_dataframe[key] = df
    
    f_dict_indicador = {key: f_dict[key] for key in f_dict if key.startswith('indicador')}
    f_dict_indicador['mes'] = mes
    f_dict_indicador['ano'] = ano
    
    return {'dataframes': f_dict_dataframe,
            'indicadores': f_dict_indicador}
    
def run_indicadores_one(year_month_tuple_list, unidade):
    
    input_path = 'tmp-data/final_output.xlsx'
    cod_empresas = np.load('tmp-data/config.npy').tolist()['cod_empresa']

    output_path = f'tmp-data/acompanhamento/acompanhamento-{cod_empresas[cod_empresas.codigo == unidade].principal_nome.iloc[0]}.xlsx'

    xl = pd.ExcelFile(input_path)
    dict_dfs = {i: xl.parse(i)
                for i in xl.sheet_names}
                
    dict_pp = preprocess_indicador(dict_dfs, unidade)
    
    df_pessoa = dict_pp['pessoa']
    df_contrato = dict_pp['contrato']
    df_mov = dict_pp['mov_contrato']
    
    indicadores_list = []
    dataframes_list = []

    for _tuple in year_month_tuple_list:
        ano, mes = _tuple
            
        dict_alunos_ativos = indicador_alunos_ativos(df_contrato, df_pessoa, mes, ano, unidade)
        dict_cancelado_ajuste = indicador_cancelado_ajuste(df_pessoa, df_mov, df_contrato, mes, ano, unidade)
        dict_novas_matriculas = indcador_nova_matriculas(df_pessoa, df_contrato, mes, ano, unidade)
        dict_rematricula_renovacao = indicador_rematricula_renovacao(df_pessoa, df_contrato, df_mov, mes, ano, unidade)
        dict_visitantes = indicador_visitantes(df_pessoa, mes, ano, unidade)
            
        dict_list = [dict_alunos_ativos, dict_cancelado_ajuste, dict_novas_matriculas, dict_rematricula_renovacao, dict_visitantes]
            
        dict_posprocess = posprocess(dict_list, mes, ano)
            
        indicadores_list.append(dict_posprocess['indicadores'])
        dataframes_list.append(dict_posprocess['dataframes'])
    
    dict_final_dataframes = {}
    indicadores = pd.DataFrame(indicadores_list)
    indicadores = indicadores[['ano', 'mes', 'indicador_alunos_ativos', 
                                'indicador_nova_matricula', 'indicador_rematricula',
                                'indicador_possiveisrenovacoes', 'indicador_renovados',
                                'indicador_nrenovados', 'indicador_cancelados', 'indicador_visitantes',
                                'indicador_ajustes']]
    dict_final_dataframes['indicadores'] = indicadores
    
    for key in dataframes_list[0].keys():
        dfs2concat = [_dict[key] for _dict in dataframes_list]
        dict_final_dataframes[key[10:]] = pd.concat(dfs2concat, sort=False)
    
    output2excel(DICT_LIST = [dict_final_dataframes],
                 output_path = output_path)
    
        
def run_indicadores(year_month_tuple_list, unidades):

    cod_empresas = np.load('tmp-data/config.npy').tolist()['cod_empresa']

    for unidade in unidades:
        logging.info('START {}'.format(cod_empresas[cod_empresas.codigo == unidade].principal_nome.iloc[0]))
        run_indicadores_one(year_month_tuple_list=year_month_tuple_list, 
                        unidade = unidade)