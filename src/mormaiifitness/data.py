import pandas as pd
import numpy as np
import logging
import sys

import mysql.connector
from mysql.connector import Error
import gspread
from oauth2client.service_account import ServiceAccountCredentials

logging.basicConfig(stream=sys.stdout,
                    level=logging.INFO,
                    format='%(asctime)s;%(levelname)s;%(message)s',
                    datefmt='%m/%d/%Y %I:%M:%S %p')

#VARIAVEIS---------------


#BASE PACTO
FILE_NAME_PACTO = '/home/gabriela/Documents/repositorios/mormaiifitness-ds/data/bdzillyonmormaiivilamariana.xlsx'
_SHEETS_LIST = [
    'contrato_0', 
    'horario_0', 
    'pessoa_0', 
    'cliente_0',
    'colaborador_0', 
    'contratomodalidade_0', 
    'modalidade_0' , 
    'contratoduracao_0', 
    'contratomodalidadevezessemana_0', 
    'turma_0', 
    'contratomodalidadeturma_0', 
    'horarioturma_0',
    'endereco_0',
    'cidade_0',
    'telefone_0',
    'email_0',
    'alunohorarioturma_0',
    'contratooperacao_0'
    
]

_SHEETS_LIST_BV = [
    'pergunta_0',
    'perguntacliente_0',
    'questionario_0',
    'questionariocliente_0',
    'questionariopergunta_0',
    'questionarioperguntacliente_0',
    'respostapergcliente_0',
    'respostapergunta_0',
]

#FACEBOOK LEADS
SHEET_NAME = 'FACEBOOK-ADS_Leads_TODOS_STUDIOS'
_SCOPE = ['https://spreadsheets.google.com/feeds',
         'https://www.googleapis.com/auth/drive']
CREDENTIALS_KEY = '/home/gabriela/Documents/repositorios/mormaiifitness-ds/data/mormaiifitness-8a226de8a311.json'

#ADTRACKER
FILE_NAME_ADTRACKER = '/home/gabriela/Documents/repositorios/mormaiifitness-ds/data/adtracker.csv'

#RD
FILE_NAME_RD = '/home/gabriela/Documents/repositorios/mormaiifitness-ds/data/rd-studio-integrado-mormaii-fitness-conversoes-ds-leads.csv'

FILE_COD_EMPRESAS = '/home/gabriela/Documents/repositorios/mormaiifitness-ds/data/cod_empresa.csv'

FILE_NAME_CONFIG = '/home/gabriela/Documents/repositorios/mormaiifitness-ds/config.xlsx'

def data_pacto(FILE_NAME_PACTO, _SHEETS_LIST, _SHEETS_LIST_BV):

    logging.info('START DATA PACTO')
    
    #LENDO ARQUIVOS DA PLANILHA
    
    xl = pd.ExcelFile(FILE_NAME_PACTO)
    dict_dfs = {i: xl.parse(i)
                for i in _SHEETS_LIST}

    dict_dfs_bv = {i: xl.parse(i)
                for i in _SHEETS_LIST_BV}
                
    #SALVANDO EM FORMATO COMPACTO
    np.save('tmp-data/base_pacto.npy', dict_dfs)
    np.save('tmp-data/base_pacto_bv.npy', dict_dfs_bv)
    
    logging.info('END DATA PACTO')
    
def data_drive(SHEET_NAME):
    
    logging.info('START DATA DRIVE')
    
    #CONFIGURANDO CREDENCIAIS
    credentials = ServiceAccountCredentials.from_json_keyfile_name(CREDENTIALS_KEY, _SCOPE)
    gc = gspread.authorize(credentials)
    
    #LENDO ARQUIVO
    sht = gc.open(SHEET_NAME).sheet1
    df = pd.DataFrame(sht.get_all_records())
    
    np.save('tmp-data/base_fbleads.npy', {'base_fbleads': df})
    logging.info('END DATA DRIVE')
 

def data_exports(FILE_NAME_RD):
    
    logging.info('START DATA EXPORTS')
    
    #LENDO ARQUIVO
    df_rd = pd.read_csv(FILE_NAME_RD)
    
    #SALVANDO EM FORMATO COMPACTO
    np.save('tmp-data/base_rd.npy', {'rd': df_rd})
    logging.info('END DATA EXPORTS')

def data_sql(user='dashboard_c19f4897', 
             password='9c5bee3b', 
             host='db.adtrackertool.com.br', 
             database='adtracker_master'):
    
    logging.info('START IMPORTING SQL')
    
    cnx = mysql.connector.connect(user=user,
                                password=password,
                                host=host,
                                database=database)
    cursor = cnx.cursor()
    
    cursor.execute("SHOW columns FROM leads_valid_c19f4897")
    columns = [column[0] for column in cursor.fetchall()]
    
    sql_select_Query = "select * from leads_valid_c19f4897"
    cursor.execute(sql_select_Query)
    records = pd.DataFrame(cursor.fetchall())
    
    records.columns = columns
    
    np.save('tmp-data/base_adtracker.npy', {'adtracker': records})
    logging.info('END IMPORTING SQL')

def data_config(FILE_NAME_CONFIG):

    logging.info('START DATA CONFIG')
    
    #LENDO ARQUIVOS DA PLANILHA
    
    xl = pd.ExcelFile(FILE_NAME_CONFIG)
    dict_dfs = {i: xl.parse(i)
                for i in xl.sheet_names}
                
    #SALVANDO EM FORMATO COMPACTO
    np.save('tmp-data/config.npy', dict_dfs)
    
    logging.info('END DATA CONFIG')
    
    
def run_data(FILE_NAME_CONFIG):

    try:
        data_config(FILE_NAME_CONFIG)
    except:
        print('Erro ao ler dados da config')


    config_dict = np.load('tmp-data/config.npy').tolist()
    path_names = config_dict['path_names']

    FILE_NAME_PACTO = path_names[path_names.nome == 'FILE_NAME_PACTO'].valor.iloc[0]
    SHEET_NAME = path_names[path_names.nome == 'SHEET_NAME'].valor.iloc[0]
    FILE_NAME_RD = path_names[path_names.nome == 'FILE_NAME_RD'].valor.iloc[0]
    
    try:
        data_pacto(FILE_NAME_PACTO, _SHEETS_LIST, _SHEETS_LIST_BV)
    except:
        print('Erro ao ler dados da pacto')
        
    try:    
        data_drive(SHEET_NAME)
    except:
        print('Erro ao ler dados do Drive')
        
    try:
        data_exports(FILE_NAME_RD)
    except:
        print('Erro ao ler dados do RD')
    
    try:
        data_sql()
    except:
        print('Erro ao ler dados do Adtracker')