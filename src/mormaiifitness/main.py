import pandas as pd
import numpy as np
import logging
import sys
from datetime import datetime

from .indicadores import *
from .data import *
from .output import *
from .preprocess import *


logging.basicConfig(stream=sys.stdout,
                    level=logging.INFO,
                    format='%(asctime)s;%(levelname)s;%(message)s',
                    datefmt='%m/%d/%Y %I:%M:%S %p')

FILE_NAME = '/home/gabriela/Documents/repositorios/mormaiifitness-ds/src/mormaiifitness/tmp-data/final_output.xlsx'
FILE_COD_EMPRESAS = '/home/gabriela/Documents/repositorios/mormaiifitness-ds/data/cod_empresa.csv'


def main_all():
    
    cod_empresas = pd.read_csv(FILE_COD_EMPRESAS)
    
    months = [1,2,3,4]
    year_month_tuple_list = [(2019, month) for month in months]
    unidades = list(cod_empresas.codigo.values)[16:]
    
    for unidade in unidades:
        logging.info('START {}'.format(cod_empresas[cod_empresas.codigo == unidade].nome_curto.iloc[0]))
        run_indicadores(year_month_tuple_list=year_month_tuple_list, 
                        unidade = unidade, 
                        input_path= FILE_NAME,
                         output_path =f'tmp-data/acompanhamento/acompanhamento-{cod_empresas[cod_empresas.codigo == unidade].nome_curto.iloc[0]}.xlsx')

def main_one():

    cod_empresas = pd.read_csv(FILE_COD_EMPRESAS)

    months = [1,2,3,4]
    year_month_tuple_list = [(2019, month) for month in months]
    unidade = 7

    logging.info('START')
    run_indicadores(year_month_tuple_list=year_month_tuple_list, 
                        unidade = unidade, 
                        input_path= FILE_NAME,
                         output_path =f'tmp-data/acompanhamento/acompanhamento-{cod_empresas[cod_empresas.codigo == unidade].nome_curto.iloc[0]}.xlsx')
    logging.info('END')

if __name__ == '__main__':

   #run_data()

    FILE_NAME_CONFIG = '/home/gabriela/Documents/repositorios/mormaiifitness-ds/config.xlsx'
    data_config(FILE_NAME_CONFIG)

    #run_preprocess(find_pessoa = True)

    #main_one()
